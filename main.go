package main

import (
	"gitlab.com/cgboal/dumpster/lib"
	"os"
	"fmt"
)

func main () {
	query := os.Args[1]
	for _, domain := range dumpster.Query(query) {
		fmt.Println(domain)
	}
}
