package dumpster

import (
	"net/http"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/url"
	"strings"
	"strconv"
)

// Query sends a POST request to DNS Dumpster to retrieve a list of Domain Names
func Query(domain string) []string {
	var results []string
	urlString := "https://dnsdumpster.com"

	csrfToken := GetCSRFToken()

	formData := url.Values{
		"csrfmiddlewaretoken": {csrfToken},
		"targetip": {domain},
	}

	r, _ := http.NewRequest("POST", urlString, strings.NewReader(formData.Encode()))

	r.AddCookie(&http.Cookie{Name: "csrftoken", Value: csrfToken,})
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Content-Length", strconv.Itoa(len(formData.Encode())))
	r.Header.Add("Referer", "https://dnsdumpster.com/")
	client := &http.Client{}
	resp, err := client.Do(r)

	if err != nil {
		log.Fatal(err)
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)

	if err != nil {
		log.Fatal(err)
	}

	doc.Find("div.table-responsive:nth-child(14) table tbody tr td.col-md-4").Each(func(index int, item *goquery.Selection) {
		results = append(results, strings.Split(item.Text(), "\n")[0])
	})

	return results

}

func GetCSRFToken() string {
	resp, _ := http.Get("https://dnsdumpster.com/")
	doc, err := goquery.NewDocumentFromReader(resp.Body)

	if err != nil {
		log.Fatal(err)
	}

	token, _ := doc.Find("input[name=csrfmiddlewaretoken]").Attr("value")
	return token
}
